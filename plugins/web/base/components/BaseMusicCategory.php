<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseMusicCategory extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseMusicCategory Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'name'        => 'BaseMusicCategory Component',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $this->page['category'] = $this->getCategory();
        $this->page['bands']    = $this->getBand();
    }

    public function getBand()
    {
        $category   = $this->getCategory();
        $categories = \Kolektivisia\Music\Models\Genre::whereParentId($category->id)->select('id')->get()->toArray();
        return \Kolektivisia\Music\Models\Band::whereIn('genre_id', $categories)->get();
    }

    public function getCategory()
    {
        return \Kolektivisia\Music\Models\Genre::whereSlug($this->property('slug'))->first();
    }
}
