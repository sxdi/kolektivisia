<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseNewsAuthor extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseNewsAuthor Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'name'        => 'BaseNewsAuthor Component',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function getBlog()
    {
        $author = $this->getAuthor();
        return \Rainlab\Blog\Models\Post::whereUserId($author->id)->orderBy('published_at', 'desc')->get();
    }

    public function getAuthor()
    {
        return \Backend\Models\User::whereLogin($this->property('slug'))->first();
    }
}
