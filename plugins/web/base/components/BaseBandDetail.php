<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseBandDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseBandDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'name'        => 'BaseBandDetail Component',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $band = $this->getBand();
        if(!$band) {
            return;
        }

        $this->page['band']          = $band;
        $this->page['relatedGenres'] = $this->getRelatedGenres();
    }

    public function getBand()
    {
        return \Kolektivisia\Music\Models\Band::whereSlug($this->property('slug'))->first();
    }

    public function getRelatedGenres()
    {
        $band   = $this->getBand();
        $genres = \Kolektivisia\Music\Models\Genre::whereParentId($band->genre->parent_id)->select('id')->get()->toArray();
        $bands  = \Kolektivisia\Music\Models\Band::whereNotIn('id', [$band->id])->whereIn('genre_id', $genres)->orderBy('id', 'desc')->take(10)->get();
        return $bands;
    }
}
