<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseMusic extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseMusic Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {

        $this->page['bands']  = $this->getBand();
        $this->page['genres'] = $this->getGenre();
    }

    public function getBand()
    {
        return \Kolektivisia\Music\Models\Band::orderBy('name')->get();
    }

    public function getGenre()
    {
        return \Kolektivisia\Music\Models\Genre::whereNull('parent_id')->orderBy('name')->get();
    }
}
