<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseNews extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseNews Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'name'        => 'BaseNews Component',
                'description' => 'No description provided yet...'
            ],
        ];
    }

    public function onRun()
    {
        $blogs = $this->getCurrent();
        if(!$blogs) {

            return;
        }

        $this->page['blogs']    = $blogs;
        $this->page['category'] = $this->getCategory();
    }

    public function getCurrent()
    {
        $category = $this->getCategory();
        return \Rainlab\Blog\Models\Post::whereHas('categories', function($q) use($category) {
            $q->whereIn('category_id', [$category->id]);
        })->orderBy('published_at', 'desc')->get();
    }

    public function getCategory()
    {
        return \Rainlab\Blog\Models\Category::whereSlug($this->property('slug'))->first();
    }
}
