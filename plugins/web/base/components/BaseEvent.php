<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseEvent extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseEvent Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'organizer' => [
                'name'        => 'BaseEvent Component',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $this->page['publications'] = $this->getPublication();
        $this->page['events']       = $this->getAll();
    }

    public function getAll()
    {
        $today = date('Y-m-d');
        if($this->property('organizer') == 'out') {
            return \Kolektivisia\Event\Models\Event::whereIsKolektivisia(0)->where('held_at', '>=', $today)->orderBy('held_at', 'asc')->get();
        }

        return \Kolektivisia\Event\Models\Event::whereIsKolektivisia(1)->where('held_at', '>=', $today)->orderBy('held_at', 'asc')->get();
    }

    public function getPublication()
    {
        if($this->property('organizer') == 'out') {
            return \Rainlab\Blog\Models\Post::orderBy('published_at', 'desc')->whereHas('event', function($q) {
                $q->whereHas('event', function($r) {
                    $r->whereIsKolektivisia(0);
                });
            })->get();
        }

        return \Rainlab\Blog\Models\Post::orderBy('published_at', 'desc')->whereHas('event', function($q) {
                $q->whereHas('event', function($r) {
                    $r->whereIsKolektivisia(1);
                });
            })->get();
    }
}
