<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseNewsCategory extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseNewsCategory Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'name'        => 'BaseNewsCategory Component',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function getBlog()
    {
        $slug = $this->property('slug');
        return \Rainlab\Blog\Models\Post::whereHas('categories', function($q) use($slug) {
            $q->where('slug', 'like', '%'.$slug.'%');
        })->orWhereHas('tags', function($q) use($slug){
            $q->where('slug', 'like', '%'.$slug.'%');
        })->orderBy('published_at', 'desc')->get();
    }
}
