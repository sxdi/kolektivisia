<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseNewsContribution extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseNewsContribution Component',
            'description' => 'No description provided yet...'
        ];
    }
    public function defineProperties()
    {
        return [];
    }

    public function onSave()
    {
        $rules = [
            'name'     => 'required',
            'email'    => 'required|email',
        ];
        $attributeNames = [
            'name'     => 'nama',
            'email'    => 'email',
        ];
        $messages       = [];

        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            \Flash::error($validator->messages()->first());
            return;
        }

        $exts       = ['doc', 'docx'];
        $attachment = \Input::file('document');

        if(!\Input::hasFile('document')) {
            \Flash::error('File tulisan harus di unggah');
            return;
        }

        // if(!in_array($attachment->getClientOriginalExtension(), $exts)) {
        //     \Flash::error('File tulisan harus berbentuk dokumen (doc/docx)');
        //     return;
        // }

        $contribution        = new \Kolektivisia\Culture\Models\Contribution;
        $contribution->name  = post('name');
        $contribution->email = post('email');
        $contribution->save();

        $file       = new \System\Models\File;
        $file->data = $attachment;
        $file->save();

        $contribution->document()->add($file);
        \Flash::success('Tulisan berhasil disimpan');
        return \Redirect::refresh();
    }
}
