<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseEventDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseEventDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'name'        => 'BaseEventDetail Component',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $event = $this->getCurrent();
        $this->page['event']             = $event;
        $this->page['eventRelated']      = $this->getEventRelated();
        $this->page['eventKolektivisia'] = $this->getRuangKolektivisia();
    }

    public function getCurrent()
    {
        return \Kolektivisia\Event\Models\Event::whereSlug($this->property('slug'))->first();
    }

    public function getRuangKolektivisia()
    {
        $event = $this->getCurrent();
        return \Kolektivisia\Event\Models\Event::whereIsKolektivisia(1)->whereNotIn('id', [$event->id])->orderBy('held_at')->take(5)->get();
    }

    public function getEventRelated($value='')
    {
        $event = $this->getCurrent();
        return \Kolektivisia\Event\Models\Event::whereIsKolektivisia(0)->whereNotIn('id', [$event->id])->orderBy('held_at')->take(5)->get();
    }
}
