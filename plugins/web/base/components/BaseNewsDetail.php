<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseNewsDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseNewsDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'name'        => 'BaseNewsDetail Component',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $blog = $this->getCurrent();
        if(!$blog) {
            return;
        }

        $this->page['blog']          = $blog;
        $this->page['trend']         = $this->getTrend();
        $this->page['authorRelated'] = $this->getAuthor();
        $this->page['blogRelated']   = $this->getRelated();
        $this->page['blogPrev']      = \Rainlab\Blog\Models\Post::where('id', '<', $blog->id)->first();
        $this->page['blogNext']      = \Rainlab\Blog\Models\Post::where('id', '>', $blog->id)->first();
    }

    public function getCurrent()
    {
        return \Rainlab\Blog\Models\Post::whereSlug($this->property('slug'))->first();
    }

    public function getTrend()
    {
        $newsManager = new \Kolektivisia\Core\Classes\NewsManager;
        return $newsManager->getTrend();
    }

    public function getAuthor()
    {
        $newsManager = new \Kolektivisia\Core\Classes\NewsManager;
        $blog        = $this->getCurrent();
        return $newsManager->getAuthorArticle($blog->user_id);
    }

    public function getRelated()
    {
        $newsManager = new \Kolektivisia\Core\Classes\NewsManager;
        $blog        = $this->getCurrent();
        return $newsManager->getRelatedArticle($blog->id);
    }
}
