<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseSearch extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseSearch Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['blogs']      = $this->getBlog();
        $this->page['categories'] = $this->getCategory();
    }

    public function getCategory()
    {
        return \Rainlab\Blog\Models\Category::get();
    }

    public function getBlog()
    {
        return \Rainlab\Blog\Models\Post::wherePublished(1)->where('title', 'like', '%'.input('keyword').'%')->take(10)->get();
    }
}
