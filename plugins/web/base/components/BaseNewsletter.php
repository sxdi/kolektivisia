<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseNewsletter extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseNewsletter Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRegister()
    {

        $newsletter = \Kolektivisia\Culture\Models\Newsletter::firstOrNew([
            'email' => post('email')
        ]);
        $newsletter->save();

        \Flash::success('Email berhasil didaftarkan');
        return \Redirect::refresh();
    }
}
