<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseHome extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseHome Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getTrend()
    {
        $newsManager = new \Kolektivisia\Core\Classes\NewsManager;
        return $newsManager->getTrend();
    }

    public function getPlaylist()
    {
        $musicManager = new \Kolektivisia\Core\Classes\MusicManager;
        return $musicManager->getPlaylist();
    }

    public function getBlogs($limit)
    {
        return \Rainlab\Blog\Models\Post::wherePublished(1)->orderBy('published_at', 'desc')->take($limit)->get();
    }

    public function getBlogBy($id)
    {
        $head = \Rainlab\Blog\Models\Post::whereHas('categories', function($q) use($id) {
            $q->where('category_id', $id);
        })->orderBy('published_at', 'desc')->first();

        $content = \Rainlab\Blog\Models\Post::whereNotIn('id', [$head->id])->whereHas('categories', function($q) use($id) {
            $q->where('category_id', $id);
        })->orderBy('published_at', 'desc')->take(4)->get();

        return [
            'head'    => $head,
            'content' => $content
        ];
    }

    public function getCategories()
    {
        return \Rainlab\Blog\Models\Category::has('posts')->orderBy('name')->get();
    }
}
