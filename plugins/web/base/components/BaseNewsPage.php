<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseNewsPage extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseNewsPage Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'category' => [
                'name'        => 'BaseNewsPage Component',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $this->page['category'] = $this->getCategory();
    }

    public function getCategory()
    {
        return \Rainlab\Blog\Models\Category::whereSlug($this->property('category'))->first();
    }

    public function getBlogPagination($categorySlug)
    {
        return \Rainlab\Blog\Models\Post::whereHas('categories', function($q) use($categorySlug) {
            $q->whereSlug($categorySlug);
        })->paginate(12);
    }
}
