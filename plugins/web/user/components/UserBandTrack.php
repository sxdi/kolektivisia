<?php namespace Web\User\Components;

use Cms\Classes\ComponentBase;

class UserBandTrack extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'UserBandTrack Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $user = $this->getUser();
        if(!$user->band) {
            \Flash::error('Kamu perlu melengkapi profile band terlebih dahulu');
            return \Redirect::to('u/band/profile');
        }
    }

    public function getUser()
    {
        return \Auth::getUser();
    }

    public function onAdd()
    {
        return [
            'track' => $this->renderPartial('account/band/form-track', [
                'key'  => post('length'),
            ])
        ];
    }

    public function onRemove()
    {
        \Kolektivisia\Music\Models\Track::whereParameter(post('parameter'))->first()->delete();
        \Flash::success('Track berhasil dihapus');
        return \Redirect::refresh();
    }

    public function onSave()
    {
        $user  = $this->getUser();
        $rules = [
            'name.*'        => 'required',
            'spotify.*'     => 'required',
        ];
        $attributeNames = [
            'name.*'        => 'setiap kolom nama',
            'spotify.*'     => 'setiap kolom spotify embeed url',
        ];
        $messages       = [];

        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            \Flash::error($validator->messages()->first());
            return;
        }

        for ($i=0; $i < count(post('name')); $i++) {
            $track = \Kolektivisia\Music\Models\Track::firstOrNew([
                'band_id'   => $user->band->id,
                'parameter' => post('parameter')[$i]
            ]);
            $track->name        = post('name')[$i];
            $track->description = post('description')[$i];
            $track->spotify     = post('spotify')[$i];
            $track->save();
        }

        \Flash::success('Track berhasil disimpan');
        return \Redirect::refresh();
    }
}
