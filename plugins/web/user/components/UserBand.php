<?php namespace Web\User\Components;

use Cms\Classes\ComponentBase;

class UserBand extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'UserBand Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function init()
    {
        $user = $this->getUser();
        if($user->band) {
            $component = $this->addComponent(
                'Responsiv\Uploader\Components\ImageUploader',
                'pictureUploader',
                ['deferredBinding' => true]
            );

            $component->bindModel('pictures', \Kolektivisia\Music\Models\Band::find($user->band->id));
        }
        else {
            $component = $this->addComponent(
                'Responsiv\Uploader\Components\ImageUploader',
                'pictureUploader',
                ['deferredBinding' => true]
            );

            $component->bindModel('pictures', new \Kolektivisia\Music\Models\Band);
        }
    }

    public function onRun()
    {
        $this->page['genres'] = $this->getGenre();
    }

    public function getUser()
    {
        return \Auth::getUser();
    }

    public function getGenre()
    {
        return \Kolektivisia\Music\Models\Genre::whereNull('parent_id')->orderBy('name')->get();
    }

    public function getGenreChild($parentId)
    {
        return \Kolektivisia\Music\Models\Genre::whereParentId($parentId)->orderBy('name')->get();
    }

    public function onSelectGenre()
    {
        $genres = \Kolektivisia\Music\Models\Genre::whereParentId(post('genre_parent'))->orderBy('name')->get();
        $this->page['childGenres'] = $genres;
        return;
    }

    public function onSave()
    {
        $user  = $this->getUser();
        $rules = [
            'genre_parent'=> 'required',
            'genre_child' => 'required',
            'name'        => 'required',
            'description' => 'required',
        ];
        $attributeNames = [
            'genre_parent' => 'genre',
            'genre_child'  => 'genre',
            'name'         => 'nama',
            'description'  => 'biografi',
        ];
        $messages       = [];

        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            \Flash::error($validator->messages()->first());
            return;
        }

        $band = \Kolektivisia\Music\Models\Band::firstOrNew([
            'user_id' => $user->id
        ]);
        $band->genre_id    = post('genre_child');
        $band->name        = post('name');
        $band->description = post('description');
        $band->socials     = [
            'spotify'   => post('social_spotify'),
            'facebook'  => post('social_facebook'),
            'instagram' => post('social_instagram'),
            'youtube'   => post('social_youtube'),
        ];
        $band->save(null, post('_session_key'));

        \Flash::success('Profile berhasil disimpan');
        return \Redirect::refresh();
    }
}
