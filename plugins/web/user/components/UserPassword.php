<?php namespace Web\User\Components;

use Cms\Classes\ComponentBase;

class UserPassword extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'UserPassword Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getUser()
    {
        return \Auth::getUser();
    }

    public function onUpdatePassword()
    {
        $user  = $this->getUser();
        $rules = [
            'password_new'    => 'required|min:6',
            'password_retype' => 'required'
        ];

        $attributeNames = [
            'password_new'    => 'password',
            'password_retype' => 'ulangi password',
        ];
        $messages = [];

        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            \Flash::error($validator->messages()->first());
            return;
        }

        $user           = \Rainlab\User\Models\User::find($user->id);
        $user->password = \Hash::make(post('password_new'));
        $user->save();
        \Auth::logout();
        \Flash::success('Password berhasil disimpan');
        return \Redirect::to('/');
    }
}
