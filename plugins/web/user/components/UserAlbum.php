<?php namespace Web\User\Components;

use Cms\Classes\ComponentBase;

class UserAlbum extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'UserAlbum Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
