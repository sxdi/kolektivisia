<?php namespace Web\User\Components;

use Cms\Classes\ComponentBase;

class UserBandMember extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'UserBandMember Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $user = $this->getUser();
        if(!$user->band) {
            \Flash::error('Kamu perlu melengkapi profile band terlebih dahulu');
            return \Redirect::to('u/band/profile');
        }
    }

    public function getUser()
    {
        return \Auth::getUser();
    }

    public function onAdd()
    {
        return [
            'member' => $this->renderPartial('account/band/form-member', [
                'key'  => post('length'),
            ])
        ];
    }

    public function onRemove()
    {
        \Kolektivisia\Music\Models\Member::whereParameter(post('parameter'))->first()->delete();
        \Flash::success('Member berhasil dihapus');
        return \Redirect::refresh();
    }

    public function onSave()
    {
        $user  = $this->getUser();
        $rules = [
            'role.*' => 'required',
            'name.*' => 'required',
        ];
        $attributeNames = [
            'role.*' => 'setiap kolom posisi',
            'name.*' => 'setiap kolom name',
        ];
        $messages       = [];

        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            \Flash::error($validator->messages()->first());
            return;
        }

        for ($i=0; $i < count(post('role')); $i++) {
            $member = \Kolektivisia\Music\Models\Member::firstOrNew([
                'band_id'   => $user->band->id,
                'parameter' => post('parameter')[$i]
            ]);
            $member->role        = post('role')[$i];
            $member->name        = post('name')[$i];
            $member->socials     = [
                'facebook'  => post('facebook')[$i],
                'twitter'   => post('twitter')[$i],
                'instagram' => post('instagram')[$i],
                'youtube'   => post('youtube')[$i],
            ];
            $member->save();
        }

        \Flash::success('Member berhasil disimpan');
        return \Redirect::refresh();
    }
}
