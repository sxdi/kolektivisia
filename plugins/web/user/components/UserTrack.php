<?php namespace Web\User\Components;

use Cms\Classes\ComponentBase;

class UserTrack extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'UserTrack Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
