<?php namespace Kolektivisia\Music;

use Backend;
use System\Classes\PluginBase;

/**
 * Music Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Music',
            'description' => 'No description provided yet...',
            'author'      => 'Kolektivisia',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Kolektivisia\Music\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'kolektivisia.music.some_permission' => [
                'tab' => 'Music',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'music' => [
                'label'       => 'Kolektivisia Musik',
                'url'         => Backend::url('kolektivisia/music/playlists'),
                'icon'        => 'icon-leaf',
                'permissions' => ['kolektivisia.music.*'],
                'order'       => 502,

                'sideMenu' => [
                    'playlists' => [
                        'label'       => 'Playlist',
                        'url'         => Backend::url('kolektivisia/music/playlists'),
                        'icon'        => 'icon-list',
                        'permissions' => ['kolektivisia.music.*'],
                        'order'       => 500,
                    ],
                    'bands' => [
                        'label'       => 'Band',
                        'url'         => Backend::url('kolektivisia/music/bands'),
                        'icon'        => 'icon-list',
                        'permissions' => ['kolektivisia.music.*'],
                        'order'       => 500,
                    ],
                    'genres' => [
                        'label'       => 'Genre',
                        'url'         => Backend::url('kolektivisia/music/genres'),
                        'icon'        => 'icon-list',
                        'permissions' => ['kolektivisia.music.*'],
                        'order'       => 500,
                    ]
                ]
            ],
        ];
    }
}
