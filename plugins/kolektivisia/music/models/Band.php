<?php namespace Kolektivisia\Music\Models;

use Model;

/**
 * Band Model
 */
class Band extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kolektivisia_music_bands';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'user_id'
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = ['socials'];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $slugs = [
        'slug' => 'name'
    ];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [
        'tracks' => [
            'Kolektivisia\Music\Models\Track',
            'key'      => 'band_id',
            'otherKey' => 'id'
        ],
        'members' => [
            'Kolektivisia\Music\Models\Member',
            'key'      => 'band_id',
            'otherKey' => 'id'
        ],
    ];
    public $belongsTo     = [
        'genre' => [
            'Kolektivisia\Music\Models\Genre',
            'key'      => 'genre_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [
        'pictures' => ['System\Models\File']
    ];

    public function getGenreOptions()
    {
        return \Kolektivisia\Music\Models\Genre::whereParentId('null')->select('id', 'name')->orderBy('name')->get()->toArray();
    }
}
