<?php namespace Kolektivisia\Music\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Genres Back-end Controller
 */
class Genres extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $bodyClass  = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kolektivisia.Music', 'music', 'genres');
    }
}
