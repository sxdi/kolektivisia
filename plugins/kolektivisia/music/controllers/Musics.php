<?php namespace Kolektivisia\Music\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Musics Back-end Controller
 */
class Musics extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kolektivisia.Music', 'music', 'musics');
    }
}
