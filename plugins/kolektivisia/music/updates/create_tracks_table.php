<?php namespace Kolektivisia\Music\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTracksTable extends Migration
{
    public function up()
    {
        Schema::create('kolektivisia_music_tracks', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('band_id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('spotify');
            $table->timestamps();
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('kolektivisia_music_tracks');
    }
}
