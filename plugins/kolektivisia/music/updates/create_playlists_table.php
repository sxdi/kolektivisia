<?php namespace Kolektivisia\Music\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePlaylistsTable extends Migration
{
    public function up()
    {
        Schema::create('kolektivisia_music_playlists', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->text('contents');
            $table->boolean('is_published')->default(0);
            $table->string('slug');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kolektivisia_music_playlists');
    }
}
