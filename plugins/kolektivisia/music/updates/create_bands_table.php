<?php namespace Kolektivisia\Music\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBandsTable extends Migration
{
    public function up()
    {
        Schema::create('kolektivisia_music_bands', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->integer('genre_id');
            $table->text('description')->nullable();
            $table->text('socials')->nullable();
            $table->string('slug');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kolektivisia_music_bands');
    }
}
