<?php namespace Kolektivisia\Tv;

use Backend;
use System\Classes\PluginBase;

/**
 * Tv Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Tv',
            'description' => 'No description provided yet...',
            'author'      => 'Kolektivisia',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Kolektivisia\Tv\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'kolektivisia.tv.some_permission' => [
                'tab' => 'Tv',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'tv' => [
                'label'       => 'Kolektivisia TV',
                'url'         => Backend::url('kolektivisia/tv/tvs'),
                'icon'        => 'icon-leaf',
                'permissions' => ['kolektivisia.tv.*'],
                'order'       => 503,

                'sideMenu' => [
                    'tvs' => [
                        'label'       => 'Playlist',
                        'url'         => Backend::url('kolektivisia/tv/tvs'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['kolektivisia.tv.*'],
                        'order'       => 500,
                    ],
                    'categories' => [
                        'label'       => 'Kategori',
                        'url'         => Backend::url('kolektivisia/tv/categories'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['kolektivisia.tv.*'],
                        'order'       => 500,
                    ],
                ]
            ],
        ];
    }
}
