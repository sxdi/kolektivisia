<?php namespace Kolektivisia\Tv\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTvCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('kolektivisia_tv_tv_categories', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('tv_id')->unsigned();
            $table->integer('category_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kolektivisia_tv_tv_categories');
    }
}
