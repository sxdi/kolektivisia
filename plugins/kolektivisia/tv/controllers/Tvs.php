<?php namespace Kolektivisia\Tv\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Tvs Back-end Controller
 */
class Tvs extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $bodyClass  = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kolektivisia.Tv', 'tv', 'tvs');
    }
}
