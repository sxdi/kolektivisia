<?php namespace Kolektivisia\Trend\Models;

use Model;

/**
 * Trend Model
 */
class Trend extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kolektivisia_trend_trends';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [
        'name' => 'required'
    ];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [
        'posts' => [
            'Rainlab\Blog\Models\Post',
            'table'    => 'kolektivisia_trend_trend_posts',
            'key'      => 'trend_id',
            'otherKey' => 'post_id'
        ]
    ];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [
        'picture' => ['System\Models\File']
    ];
    public $attachMany    = [];

    public function getPostsOptions()
    {
        $blogs = \Rainlab\Blog\Models\Post::wherePublished(1)->OrderBy('title')->get();
        $options = [];
        foreach ($blogs as $blog) {
            array_push($options, [
                $blog->title
            ]);
        }

        return $options;
    }
}
