<?php namespace Kolektivisia\Trend\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTrendsTable extends Migration
{
    public function up()
    {
        Schema::create('kolektivisia_trend_trends', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->boolean('is_published')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kolektivisia_trend_trends');
    }
}
