<?php namespace Kolektivisia\Trend\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTrendPostsTable extends Migration
{
    public function up()
    {
        Schema::create('kolektivisia_trend_trend_posts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('trend_id')->unsigned();
            $table->integer('post_id')->unsigned();
            $table->primary(['trend_id', 'post_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('kolektivisia_trend_trend_posts');
    }
}
