<?php namespace Kolektivisia\Radio;

use Backend;
use System\Classes\PluginBase;

/**
 * Radio Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Radio',
            'description' => 'No description provided yet...',
            'author'      => 'Kolektivisia',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Kolektivisia\Radio\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'kolektivisia.radio.some_permission' => [
                'tab' => 'Radio',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'radio' => [
                'label'       => 'Kolektivisia Radio',
                'url'         => Backend::url('kolektivisia/radio/radios'),
                'icon'        => 'icon-leaf',
                'permissions' => ['kolektivisia.radio.*'],
                'order'       => 504,

                'sideMenu' => [
                    'radios' => [
                        'label'       => 'Radio',
                        'url'         => Backend::url('kolektivisia/radio/radios'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['kolektivisia.radio.*'],
                        'order'       => 500,
                    ],
                    'categories' => [
                        'label'       => 'Kategori',
                        'url'         => Backend::url('kolektivisia/radio/categories'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['kolektivisia.radio.*'],
                        'order'       => 500,
                    ],
                ]
            ],
        ];
    }
}
