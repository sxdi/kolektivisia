<?php namespace Kolektivisia\Radio\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Radios Back-end Controller
 */
class Radios extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $bodyClass  = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kolektivisia.Radio', 'radio', 'radios');
    }
}
