<?php namespace Kolektivisia\Radio\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRadioCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('kolektivisia_radio_radio_categories', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('radio_id')->unsigned();
            $table->integer('category_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kolektivisia_radio_radio_categories');
    }
}
