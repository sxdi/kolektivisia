<?php namespace Kolektivisia\Culture\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePickPostsTable extends Migration
{
    public function up()
    {
        Schema::create('kolektivisia_culture_pick_posts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('pick_id')->unsigned();
            $table->integer('post_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kolektivisia_culture_pick_posts');
    }
}
