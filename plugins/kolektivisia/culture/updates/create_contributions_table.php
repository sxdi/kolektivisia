<?php namespace Kolektivisia\Culture\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateContributionsTable extends Migration
{
    public function up()
    {
        Schema::create('kolektivisia_culture_contributions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kolektivisia_culture_contributions');
    }
}
