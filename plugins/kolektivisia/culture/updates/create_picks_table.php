<?php namespace Kolektivisia\Culture\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePicksTable extends Migration
{
    public function up()
    {
        Schema::create('kolektivisia_culture_picks', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->timestamps();
            $table->boolean('is_published')->nullable()->default(0);
            $table->string('slug');
        });
    }

    public function down()
    {
        Schema::dropIfExists('kolektivisia_culture_picks');
    }
}
