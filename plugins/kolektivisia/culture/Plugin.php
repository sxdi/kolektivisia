<?php namespace Kolektivisia\Culture;

use Backend;
use System\Classes\PluginBase;

/**
 * Culture Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Culture',
            'description' => 'No description provided yet...',
            'author'      => 'Kolektivisia',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Kolektivisia\Culture\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'kolektivisia.culture.some_permission' => [
                'tab' => 'Culture',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'culture' => [
                'label'       => 'Culture',
                'url'         => Backend::url('kolektivisia/culture/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['kolektivisia.culture.*'],
                'order'       => 500,
            ],
        ];
    }
}
