<?php namespace Kolektivisia\Core;

use Backend;
use System\Classes\PluginBase;

/**
 * Core Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Core',
            'description' => 'No description provided yet...',
            'author'      => 'Kolektivisia',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConsoleCommand('kolektivisia.up', 'Kolektivisia\Core\Console\SampleData');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Kolektivisia\Core\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'kolektivisia.core.some_permission' => [
                'tab' => 'Core',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'core' => [
                'label'       => 'Kolektivisia',
                'url'         => Backend::url('kolektivisia/core/picks'),
                'icon'        => 'icon-leaf',
                'permissions' => ['kolektivisia.core.*'],
                'order'       => 501,

                'sideMenu' => [
                    'picks' => [
                        'label'       => 'Picks',
                        'url'         => Backend::url('kolektivisia/core/picks'),
                        'icon'        => 'icon-line-chart',
                        'permissions' => ['kolektivisia.core.*'],
                        'order'       => 500,
                    ],
                    'contributions' => [
                        'label'       => 'Kontribusi',
                        'url'         => Backend::url('kolektivisia/core/contributions'),
                        'icon'        => 'icon-file',
                        'permissions' => ['kolektivisia.core.*'],
                        'order'       => 500,
                    ],
                    'newsletters' => [
                        'label'       => 'Berlangganan',
                        'url'         => Backend::url('kolektivisia/core/newsletters'),
                        'icon'        => 'icon-envelope',
                        'permissions' => ['kolektivisia.core.*'],
                        'order'       => 500,
                    ]
                ]
            ],
        ];
    }
}
