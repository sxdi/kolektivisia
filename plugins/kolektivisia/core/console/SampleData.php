<?php namespace Kolektivisia\Core\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SampleData extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'kolektivisia:up';

    /**
     * @var string The console command description.
     */
    protected $description = 'Does something cool.';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $data = [
            [
                'name'   => 'ELECTRONIC',
                'childs' => [
                    'Drum & Bass',
                    'EDM',
                    'Electro Pop',
                    'House',
                    'Techno',
                    'Electro',
                    'Trance',
                    'Dubstep',
                    'Ambient',
                    'Industrial',
                    'Acid House',
                    'Trip Hop',
                    'Experimental',
                    'Rave',
                ]
            ],[
                'name'   => 'FOLK',
                'childs' => [
                    'Ballad',
                    'Orkes',
                    'Folk',
                    'Folk Jazz',
                    'World Music',
                    'Keroncong',
                ]
            ],[
                'name'   => 'HARDCOE',
                'childs' => [
                    'Hardcore Punk',
                    'Post Hardcore',
                    'Hardcore',
                ]
            ], [
                'name'   => 'JAZZ',
                'childs' => [
                    'Acid Jazz',
                    'Funk',
                    'Soul Jazz',
                    'Swing Jazz',
                    'RnB',
                    'Jazz',
                ]
            ], [
                'name'   => 'METAL',
                'childs' => [
                    'Black Metal',
                    'Death Metal',
                    'Heavy Metal',
                    'Metal',
                    'Trash Metal',
                    'Gothic Metal',
                    'Symphonic Metal',
                    'Deathcore',
                    'Hip Metal',
                    'Nu Metal',
                    'Progressive Metal',
                    'Power Metal',
                ]
            ], [
                'name'   => 'POP',
                'childs' => [
                    'Pop',
                    'Indiepop',
                    'Pop Jazz',
                    'Psychedelic Pop',
                    'Progressive Pop',
                    'Surf Pop',
                    'Pop Rock',
                ]
            ], [
                'name'   => 'PUNK',
                'childs' => [
                    'Grindcore',
                    'Melodic Punk',
                    'Pop Punk',
                    'Punk rock',
                    'Ska Punk',
                    'Punk',
                    'Post-punk',
                ]
            ], [
                'name'   => 'rock',
                'childs' => [
                    'Alt. Rock',
                    'Grunge',
                    'Post Rock',
                    'Progressive Rock',
                    'Rock n Roll',
                    'Shoegaze',
                    'Rock',
                    'Blues Rock',
                    'Hard Rock',
                    'Art Rock',
                    'Surf Rock',
                    'Psychedelic Rock',
                    'Indie Rock',
                    'Hip Rock',
                    'Rockabilly',
                    'Heavy Rock',
                ]
            ], [
                'name'   => 'SKA',
                'childs' => [
                    'Ska',
                    'Reggae',
                    'Rock Steady',
                ]
            ]
        ];

        foreach ($data as $d) {
            $genre = \Kolektivisia\Music\Models\Genre::firstOrCreate([
                'name' => $d['name']
            ]);
            $genre->save();

            foreach ($d['childs'] as $child) {
                $child = \Kolektivisia\Music\Models\Genre::firstOrCreate([
                    'parent_id' => $genre->id,
                    'name'      => $child,
                ]);
                $child->save();
            }
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
