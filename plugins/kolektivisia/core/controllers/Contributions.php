<?php namespace Kolektivisia\Core\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Contributions Back-end Controller
 */
class Contributions extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kolektivisia.Core', 'core', 'contributions');
    }
}
