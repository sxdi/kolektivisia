<?php namespace Kolektivisia\Core\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Trends Back-end Controller
 */
class Trends extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $bodyClass  = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Kolektivisia.Core', 'core', 'trends');
    }
}
