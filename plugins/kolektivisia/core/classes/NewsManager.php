<?php namespace Kolektivisia\Core\Classes;

class NewsManager
{

    public function getTrend()
    {
        $pick = \Kolektivisia\Culture\Models\Pick::orderBy('id', 'desc')->whereIsPublished(1)->first();
        if($pick) {
            $posts = \Kolektivisia\Culture\Models\PickPost::wherePickId($pick->id)->orderBy('post_id', 'desc')->get();
            return $posts;
        }
    }

    public function getAuthorArticle($authorId)
    {
        $blogs = \Rainlab\Blog\Models\Post::wherePublished(1)->whereIn('user_id', [$authorId])->take(5)->get();
        return $blogs;
    }

    public function getRelatedArticle($blogId)
    {
        $blog       = \Rainlab\Blog\Models\Post::find($blogId);
        $categories = [];

        if($blog->categories) {
            foreach ($blog->categories as $category) {
                array_push($categories, $category->id);
            }
        }

        $blogs      = \Rainlab\Blog\Models\Post::whereNotIn('id', [$blog->id])->whereHas('categories', function($q) use($categories){
            $q->whereIn('category_id', $categories);
        })->take(5)->get();

        return $blogs;
    }
}
