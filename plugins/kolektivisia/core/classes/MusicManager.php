<?php namespace Kolektivisia\Core\Classes;

class MusicManager
{

    public function getPlaylist()
    {
        return \Kolektivisia\Music\Models\Playlist::orderBy('created_at', 'desc')->whereIsPublished(1)->first();
    }

    public function getGenres()
    {
        return \Kolektivisia\Music\Models\Genre::whereNull('parent_id')->orderBy('name')->get();
    }

    public function getBands($orderBy)
    {
        return \Kolektivisia\Music\Models\Band::orderBy($orderBy)->get();
    }
}
