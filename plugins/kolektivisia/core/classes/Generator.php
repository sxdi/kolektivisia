<?php namespace Kolektivisia\Core\Classes;

class Generator
{
    public function make()
    {
        return md5(\Hash::make(rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).date('Y-m-d H:i:s')));
    }
}
