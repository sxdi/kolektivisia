<?php namespace Kolektivisia\Store\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductTagsTable extends Migration
{
    public function up()
    {
        Schema::create('kolektivisia_store_product_tags', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('product_id')->unsigned();
            $table->integer('tag_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kolektivisia_store_product_tags');
    }
}
