<?php namespace Kolektivisia\Store;

use Backend;
use System\Classes\PluginBase;

/**
 * Store Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Store',
            'description' => 'No description provided yet...',
            'author'      => 'Kolektivisia',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Kolektivisia\Store\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'kolektivisia.store.some_permission' => [
                'tab' => 'Store',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'store' => [
                'label'       => 'Kolektivisia Store',
                'url'         => Backend::url('kolektivisia/store/products'),
                'icon'        => 'icon-leaf',
                'permissions' => ['kolektivisia.store.*'],
                'order'       => 505,

                'sideMenu' => [
                    'products' => [
                        'label'       => 'Produk',
                        'url'         => Backend::url('kolektivisia/store/products'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['kolektivisia.store.*'],
                        'order'       => 500,
                    ],
                    'categories' => [
                        'label'       => 'Kategori',
                        'url'         => Backend::url('kolektivisia/store/categories'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['kolektivisia.store.*'],
                        'order'       => 500,
                    ]
                ]
            ],
        ];
    }
}
