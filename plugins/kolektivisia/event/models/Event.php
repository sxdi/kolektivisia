<?php namespace Kolektivisia\Event\Models;

use Model;

/**
 * Event Model
 */
class Event extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'kolektivisia_event_events';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [
        'organizer' => 'required',
        'held_at'   => 'required|date',
        'name'      => 'required',
        'place'     => 'required',
        'content'   => 'required'
    ];

    public $customMessages = [
        'organizer' => 'penyelenggara',
        'held_at'   => 'tanggal',
        'name'      => 'nama',
        'place'     => 'tempat',
        'content'   => 'konten'
    ];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'held_at',
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [];
    public $belongsToMany = [
        'blogs' => [
            'Rainlab\Blog\Models\Post',
            'table'    => 'kolektivisia_event_event_blogs',
            'key'      => 'event_id',
            'otherKey' => 'blog_id'
        ]
    ];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [
        'picture' => ['System\Models\File']
    ];
    public $attachMany    = [];

    public function beforeSave()
    {
        $this->slug = str_slug($this->name);
    }
}
