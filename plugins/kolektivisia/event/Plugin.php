<?php namespace Kolektivisia\Event;

use Backend;
use System\Classes\PluginBase;

/**
 * Event Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Event',
            'description' => 'No description provided yet...',
            'author'      => 'Kolektivisia',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Kolektivisia\Event\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'kolektivisia.event.some_permission' => [
                'tab' => 'Event',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        // return []; // Remove this line to activate

        return [
            'event' => [
                'label'       => 'Kolektivisia Event',
                'url'         => Backend::url('kolektivisia/event/events'),
                'icon'        => 'icon-leaf',
                'permissions' => ['kolektivisia.event.*'],
                'order'       => 501,

                'sideMenu' => [
                    'events' => [
                        'label'       => 'Events',
                        'url'         => Backend::url('kolektivisia/event/events'),
                        'icon'        => 'icon-leaf',
                        'permissions' => ['kolektivisia.event.*'],
                        'order'       => 501,
                    ]
                ]
            ],
        ];
    }
}
