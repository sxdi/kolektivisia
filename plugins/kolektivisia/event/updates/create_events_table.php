<?php namespace Kolektivisia\Event\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateEventsTable extends Migration
{
    public function up()
    {
        Schema::create('kolektivisia_event_events', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('held_at');
            $table->string('organizer')->nullable();
            $table->string('name');
            $table->string('place');
            $table->text('content');
            $table->boolean('is_published')->nullable()->default(0);
            $table->boolean('is_kolektivisia')->nullable()->default(0);
            $table->timestamps();
            $table->string('slug');
        });
    }

    public function down()
    {
        Schema::dropIfExists('kolektivisia_event_events');
    }
}
