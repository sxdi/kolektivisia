<?php namespace Kolektivisia\Event\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateEventBlogsTable extends Migration
{
    public function up()
    {
        Schema::create('kolektivisia_event_event_blogs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('event_id');
            $table->integer('blog_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('kolektivisia_event_event_blogs');
    }
}
