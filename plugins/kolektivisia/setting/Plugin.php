<?php namespace Kolektivisia\Setting;

use Backend;
use System\Classes\PluginBase;

/**
 * Setting Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Setting',
            'description' => 'No description provided yet...',
            'author'      => 'Kolektivisia',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        \Event::listen('cms.page.beforeDisplay', function($controller, $page, $url) {
            $controller->vars['settings'] = [
                'site'       => \Kolektivisia\Setting\Models\Setting::instance(),
                'appearance' => \Kolektivisia\Setting\Models\Appearance::instance(),
            ];
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Kolektivisia\Setting\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'kolektivisia.setting.some_permission' => [
                'tab' => 'Setting',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'setting' => [
                'label'       => 'Setting',
                'url'         => Backend::url('kolektivisia/setting/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['kolektivisia.setting.*'],
                'order'       => 500,
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Website',
                'description' => 'Manage available website settings.',
                'category'    => 'Website',
                'icon'        => 'icon-desktop',
                'class'       => 'Kolektivisia\Setting\Models\Setting',
                'order'       => 100,
            ],
            'appearance' => [
                'label'       => 'Appearance',
                'description' => 'Manage available website appearance settings.',
                'category'    => 'Website',
                'icon'        => 'icon-image',
                'class'       => 'Kolektivisia\Setting\Models\Appearance',
                'order'       => 101,
            ],
        ];
    }
}
